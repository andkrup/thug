<?php
/**
 * Created by PhpStorm.
 * User: ak
 * Date: 27/03/16
 * Time: 23:24
 */
namespace supervillainhq\thugs\cli {

	class CliOutputTag {
		protected $bold;
		protected $foreground;
		protected $background;

		function __construct($bold = false, $foreground = null, $background = null) {
			$this->bold = $bold;
			$this->background = $background;
			$this->foreground = $foreground;
		}

		function bold(){
			return $this->bold;
		}
		function foreground(){
			return $this->foreground;
		}
		function background(){
			return $this->background;
		}
	}
}
