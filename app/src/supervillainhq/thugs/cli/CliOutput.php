<?php
/**
 * Created by ak.
 */

namespace supervillainhq\thugs\cli{
	class CliOutput {
		const BLACK = 0;
		const RED = 1;
		const GREEN = 2;
		const YELLOW = 3;
		const BLUE = 4;
		const MAGENTA = 5;
		const CYAN = 6;
		const WHITE = 7;

		private $tags = [];

		function __construct($tags = null) {
			if(is_null($tags)){
				$tags = (object) [
					'header1' => new CliOutputTag(true, self::WHITE, self::BLACK),
					'header2' => new CliOutputTag(false, self::YELLOW, self::BLACK),
					'emphasis' => new CliOutputTag(true, self::BLUE, self::CYAN),
					'hilite' => new CliOutputTag(false, null, self::YELLOW),
					'code' => new CliOutputTag(false, self::GREEN),
					'bold' => new CliOutputTag(true),
				];
			}
			$this->tags = $tags;
		}

		private static function foregroundColor($foreground = null){
			if(is_numeric($foreground)){
				return intval($foreground) + 30;
			}
			return '';
		}
		private static function backgroundColor($background = null){
			if(is_numeric($background)){
				return intval($background) + 40;
			}
			return '';
		}

		private static function attributes(CliOutputTag $tag){
			$attributes = [];
			if($tag->bold()){
				array_push($attributes, 1);
			}
			$foreground = $tag->foreground();
			if(!is_null($foreground)){
				array_push($attributes, self::foregroundColor($foreground));
			}
			$background = $tag->background();
			if(!is_null($background)){
				array_push($attributes, self::backgroundColor($background));
			}
			return $attributes;
		}

		private function start($tag){
			if(property_exists($this->tags, $tag)){
				$cliOutputTag = $this->tags->{$tag};
				$attributes = self::attributes($cliOutputTag);
				$attributes = implode(';', $attributes);
				return "\033[{$attributes}m";
			}
			return $tag;
		}

		function line($text = ''){
			preg_match_all('/<([a-z0-9]*)>/', $text, $matches);
			$tags = $matches[0];
			$tagnames = $matches[1];

			$c = count($tags);
			for($i = 0; $i < $c; $i++){
				$tag = $tags[$i];
				$tagname = $tagnames[$i];
				$tagReplacement = $this->start($tagname);
				$text = str_replace($tag, $tagReplacement, $text);
				$text = str_replace("</{$tagname}>", "\033[0m", $text);
			}
			echo "{$text}\n";
		}
	}
}
