<?php
/**
 * Created by ak.
 */
namespace supervillainhq\thugs\cli\ssh {

	use supervillainhq\henchmen\cli\Optioning;

	class SSH {
		use Optioning;

		protected $user;
		protected $host;
		protected $path;
//		protected $options;
		protected $identityFile;
		protected $eCommand;
		protected $payload;


		function user( $user = null){
			if(!is_null($user)){
				$this->user = $user;
			}
			return $this->user;
		}
		function host( $host = null){
			if(!is_null($host)){
				$this->host = $host;
			}
			return $this->host;
		}
		function path( $path = null){
			if(!is_null($path)){
				$this->path = $path;
			}
			return $this->path;
		}
		function identity( $identity = null){
			if(!is_null($identity)){
				$this->identityFile = $identity;
			}
			return $this->identityFile;
		}

		private function buildSshCommand(){
			$template = <<<TEMPLATE
ssh :user@:host:path :identity <<<SSH
:payload
SSH
TEMPLATE;

			$command = str_replace(':user', $this->user, $template);
			$command = str_replace(':host', $this->host, $command);
			$command = str_replace(':path', $this->path, $command);
			$command = str_replace(':identity', $this->identityFile, $command);
			return $command;
		}


		function resetPayload(array $payload = []){
			$this->payload = $payload;
		}
		function addPayload( $payload){
			array_push($this->payload, $payload);
		}
		function removePayload( $payload){
			$l = count($this->payload);
			for($i = 0; $i < $c; $i++){
				if($payload->equals($this->payload[$i])){
					array_splice($this->payload, $i, 1);
					return;
				}
			}
		}
		function removePayloadAt($index){
			unset($this->payload[$index]);
		}
		function getPayload($index){
			return $this->payload[$index];
		}
		function indexOfPayload( $payload){
			return -1;
		}
		function hasPayload( $payload){
			foreach($this->payload as $instance){
				if($instance->equals($payload)){
					return true;
				}
			}
			return false;
		}

		function payloads(){
			return $this->payload;
		}
	}
}
