<?php

namespace supervillainhq\thugs{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\lang\ClassName;

	/**
	 * Create or manage model classes
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class JsonFIleTask extends FileTask{

		private $template = <<<TEMPLATE
<?php
/**
 * Created by thugs
 *
 */
namespace :namespace{
	use supervillainhq\phalcon\db\DataReader;
:uses

	:class{
:fields

		static function inflate(DataReader \$reader){
			\$instance = new :name();
			\$instance->id = \$reader->id;
			return \$instance;
		}
	}
}
TEMPLATE;


		private $imports;
		private $classDefinition;


		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		public function createAction(array $parameters = null){
			$this->resetImports();
			$config = $this->getDI()->getConfig();

			$fullname = $parameters[0];
			$className = new ClassName($fullname);
			$this->pathinfo($className->pathinfo($config->application->srcDir));

			$ns = $className->getNamespace();
			$superclasses = count($parameters) > 1 ? (strlen($parameters[1]) > 0 ? explode(',', $parameters[1]) : null) : null;
			$interfaces = count($parameters) > 2 ? (strlen($parameters[2]) > 0 ? explode(',', $parameters[2]) : null) : null;
			$traits = count($parameters) > 3 ? (strlen($parameters[3]) > 0 ? explode(',', $parameters[3]) : null) : null;

			$contents = str_replace(':namespace', $ns, $this->template);
			$contents = str_replace(':name', $className->getClassname(), $contents);

			$this->classDefinition = $this->buildClassDefinition($className, $superclasses, $interfaces);
			$contents = str_replace(':class', $this->classDefinition, $contents);

			// fill in fields and traits
			$fields = $this->buildFields([], $traits);
			$contents = str_replace(':fields', $fields, $contents);

			// fill in import statements
			$imports = [];
			foreach ($this->imports as $import){
				$name = $import->fullname();
				array_push($imports, "\tuse {$name};");
			}
			$contents = str_replace(':uses', implode('\n', $imports), $contents);

			$this->writeToFile($contents);
		}
		
		protected function buildClassDefinition(ClassName $class, array $superclasses = null, array $interfaces = null){
			$cn = $class->getClassname();

			$definition = (object) [
				'inheriting' => null,
				'implementing' => null,
			];
			if(!empty($superclasses)){
				$definition->inheriting = [];
				foreach ($superclasses as $def){
					$super = new ClassName($def);
					array_push($definition->inheriting, $super->getClassname());
					if(!$super->inRootNS()){
						$this->addImport($super);
					}
				}
				$definition->inheriting = ' extends ' . implode(', ', $definition->inheriting);
			}

			if(!empty($interfaces)){
				$definition->implementing = [];
				foreach ($interfaces as $def){
					$iface = new ClassName($def);
					array_push($definition->implementing, $iface->getClassname());
					if(!$iface->inRootNS()){
						$this->addImport($iface);
					}
				}
				$definition->implementing = ' implements ' . implode(', ', $definition->implementing);
			}
			return "class {$cn}{$definition->inheriting}{$definition->implementing}";
		}

		protected function buildFields(array $fields = [], array $traits = null){
			if(!empty($traits)){
				$usings = [];
				foreach ($traits as $trait){
					$trait = new ClassName($trait);
					array_push($usings, $trait->getClassname());
					if(!$trait->inRootNS()){
						$this->addImport($trait);
					}
				}
				return "\t\tuse " . implode(";\n", $usings) . ";\n";
			}
			return '';
		}


		function resetImports(array $imports = []){
			$this->imports = $imports;
		}
		function addImport(ClassName $import){
			array_push($this->imports, $import);
		}
		function removeImport(ClassName $import){
			$c = count($this->imports);
			for($i = 0; $i < $c; $i++){
				if($import->equals($this->imports[$i])){
					array_splice($this->imports, $i, 1);
					return;
				}
			}
		}
		function removeImportAt($index){
			unset($this->imports[$index]);
		}
		function importAt($index){
			return $this->imports[$index];
		}
		function indexOfImport(ClassName $import){
			return -1;
		}
		function hasImport(ClassName $import){
			foreach($this->imports as $instance){
				if($instance->equals($import)){
					return true;
				}
			}
			return false;
		}

		function imports(){
			return $this->imports;
		}
	}
}
