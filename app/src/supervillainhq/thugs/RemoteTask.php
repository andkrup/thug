<?php

namespace supervillainhq\thugs{
	use Phalcon\Cli\Task;
	use supervillainhq\core\io\FileEditor;

	/**
	 * Execute a task on a remove environment via ssh
	 *
	 * Created by ak
	 */
	class RemoteTask extends Task{
		protected $ssh;
		protected $pathinfo;

		function pathinfo($pathinfo = null){
			if(!is_null($pathinfo)){
				$this->pathinfo = (object) $pathinfo;
			}
			return $this->pathinfo;
		}

		protected function createPath($pathinfo = null){
			if(!is_null($pathinfo)){
				$this->pathinfo($pathinfo);
			}
			$dirname = property_exists($this->pathinfo, 'dirname') ? $this->pathinfo->dirname : '';
			$basename = property_exists($this->pathinfo, 'basename') ? $this->pathinfo->basename : '';
			$writer = FileEditor::create("{$dirname}/{$basename}");
			$writer->mkdir(false);
		}

		protected function writeToFile($contents, $pathinfo = null){
			if(!is_null($pathinfo)){
				$this->pathinfo($pathinfo);
			}
			$dirname = property_exists($this->pathinfo, 'dirname') ? $this->pathinfo->dirname : '';
			$basename = property_exists($this->pathinfo, 'basename') ? $this->pathinfo->basename : '';
			$writer = FileEditor::create("{$dirname}/{$basename}");
			$writer->contents($contents);
			$writer->write();
			$fileCreated = realpath("{$dirname}/{$basename}");
			echo "\nFile created: {$fileCreated}\n";
		}
	}
}
