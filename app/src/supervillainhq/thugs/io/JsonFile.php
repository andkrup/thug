<?php
/**
 * Created by PhpStorm.
 * User: ak
 * Date: 27/03/16
 * Time: 19:33
 */

namespace supervillainhq\thugs\io{
	class JsonFile {
		protected $filepath;
		protected $contents;

		private function __construct($filepath){
			$this->filepath = $filepath;
		}

		protected function parse(){
			$this->contents = json_decode(file_get_contents($this->filepath));
		}

		static function load($filepath){
			$instance = new JsonFile($filepath);
			$instance->parse();
			return $instance;
		}

		function add($key, $value){
			$this->contents[$key] = $value;
		}

		function remove($key){
			unset($this->contents[$key]);
		}

		function has($key){
			return isset($this->contents[$key]);
		}

		function update(){
			file_put_contents($this->filepath, json_encode($this->contents));
		}
	}
}