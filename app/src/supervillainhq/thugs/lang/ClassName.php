<?php
/**
 * Created by PhpStorm.
 * User: ak
 * Date: 24/03/16
 * Time: 14:52
 */

namespace supervillainhq\thugs\lang{
	class ClassName {
		private $name;
		private $classname;
		private $namespace;

		function __construct($name) {
			$this->name = $name;
			$frags = explode("\\", $name);
			$this->classname = array_pop($frags);
			$this->namespace = implode('\\', $frags);

		}

		function getNamespace(){
			return $this->namespace;
		}

		function inRootNS(){
			return $this->getNamespace() == '';
		}

		function inNamespace($namespace){
			return strpos($this->namespace, $namespace) >= 0;
		}

		function getClassname(){
			if($this->inRootNS()){
				return "\\{$this->classname}";
			}
			return $this->classname;
		}

		function fullname(){
			return $this->name;
		}

		function pathinfo($abspath = ''){
			$file = str_replace('\\', '/', $this->name);
			$dirname = dirname("{$abspath}/{$file}");
			$filename = basename("{$file}");
			return (object) pathinfo("{$dirname}/{$filename}.php");
		}
	}
}
