<?php

namespace supervillainhq\thugs\oddjob {
	use Phalcon\Loader;
	use supervillainhq\thugs\oddjobtasks\TestTask;

	/**
	 * Created by ak.
	 */
	class Oddjob {
		private static $instance;
		private $vendorPath;
		private $loader;

		static function instance(){
			if(!self::$instance){
				self::$instance = new Oddjob();
			}
			return self::$instance;
		}

		private function __construct() {
		}

		function loader(Loader $loader){
			$this->loader = $loader;
		}

		function vendorPath($path){
			$this->vendorPath = $path;
			$distributors = new \IteratorIterator(new \DirectoryIterator($path));
			foreach ($distributors as $distributor){
				if($distributor->isDir() && !$distributor->isDot()){
					$this->scan($distributor->getPathname());
				}
			}
		}

		/**
		 * Scan a file path for oddjob.json files
		 * @param $path
		 */
		function scan($path){
			$oddjobCfg = $path . "/oddjob.json";
			if(is_readable($oddjobCfg)){
				$this->readConfig($oddjobCfg);
			}
			$iterator = new \IteratorIterator(new \DirectoryIterator($path));
			foreach ($iterator as $file){
				if($file->isDir() && !$file->isDot()){
					$oddjobCfg = $file->getPathname() . "/oddjob.json";
					if(is_readable($oddjobCfg)){
						$this->readConfig($oddjobCfg);
					}
				}

			}
		}

		private function readConfig($configFilepath){
			$data = json_decode(file_get_contents($configFilepath));
			$namespaces = (array) $data->namespaces;
			$namespaces = array_merge($this->loader->getNamespaces(), $namespaces);

			$this->loader->registerNamespaces($namespaces);
			$this->loader->register();
		}

		/**
		 * Tries to search every namespace in the loader for a task class file that matches the requested task.
		 *
		 * @param $arg the task basename (without -Task suffix, file extension and no paths)
		 * @return Returns the classpath for the found task class or null
		 */
		function findTask($arg){
			$namespaces = $this->loader->getNamespaces();
			foreach ($namespaces as $namespace => $path){
				$classfilename = strtolower("{$arg}task.php");
				foreach (glob($path . '/*')  as $file){
					$filename = pathinfo($file, PATHINFO_BASENAME);
					if(strtolower($filename) == $classfilename){
						$classname = pathinfo($file, PATHINFO_FILENAME);
						return "{$namespace}\\{$classname}";
					}
				}
			}
			return null;
		}

		/**
		 * Tries to instantiate a found task classpath
		 *
		 * @param $taskname
		 * @return mixed
		 */
		function getTaskInstance($taskname){
			$classname = $this->findTask($taskname);
			$this->loader->autoLoad($classname);
			return new $classname();
		}
	}
}
