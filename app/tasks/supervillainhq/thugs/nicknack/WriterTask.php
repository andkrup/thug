<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\FileTask;
	use supervillainhq\thugs\lang\ClassName;

	/**
	 * Create or manage datawriter classes
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class WriterTask extends FileTask{

		private $template = <<<TEMPLATE
<?php
/**
 * Created by thugs
 *
 */
namespace :namespace{
	use supervillainhq\phalcon\db\DataWriter;
	use supervillainhq\phalcon\db\ParameterContainer;
	use supervillainhq\phalcon\db\Reading;
	use supervillainhq\phalcon\db\SqlQuery;
:uses

	:class implements DataWriter{
		use ParameterContainer, Reading;

		function __construct(\$data){
			if(!is_null(\$data)){
				if(\$data instanceof :model){
					\$this->addParameter('id', \$data->id());
				}
				elseif (is_array(\$data)){
					\$this->resetParameters();
					\$keys = array_keys(\$data);
					foreach (\$keys as \$key){
						\$k = str_ireplace(':prefix_', '', \$key);
						\$this->addParameter(\$k, \$data[\$key]);
					}
				}
			}
		}
		
		function create(){
			\$sql = "insert into :table
					(:columns)
					values (:placeholders);";
			\$parameters = [
:params
			];
			\$query = SqlQuery::create(\$sql);
			if(\$query->execute(\$parameters)){
				return \$query->lastInsertId();
			}
			throw new \Exception('failed to create row');
		}
		
		function update(){
			\$sql = "update :table set
:sets
					where id = :id;";
			\$parameters = [
:params
					'id' => \$this->id
			];
			\$query = SqlQuery::create(\$sql);
			return \$query->execute(\$parameters);
		}
		
		function delete(){
			\$sql = "delete from :table
					where id = :id;";
			\$parameters = [
				'id' => \$this->id
			];
			\$query = SqlQuery::create(\$sql);
			return \$query->execute(\$parameters);
		}

		function __get(\$name){
			if(\$this->hasParameterAtKey(\$name)){
				switch(\$name){
					case 'id':
						return intval(\$this->getParameter(\$name));
:cases
						return stripslashes(trim(\$this->getParameter(\$name)));
				}
			}
			return null;
		}
	}
}
TEMPLATE;


		private $imports;
		private $classDefinition;

		private $modelClass;


		public function mainAction(){
			echo "\nUsage: thugs writer <model-class> <table-name> [column,...]\n";
		}

		public function helpAction(){
			echo "\nCreates a writer class file. The model doesn't need to exist\n";
		}

		public function createAction(array $parameters = null){
			$this->resetImports();
			$config = $this->getDI()->getConfig();

			$modelName = $parameters[0];
			$this->modelClass = new ClassName($modelName);
			$this->addImport($this->modelClass);

			$writerName = new ClassName($this->modelClass->getNamespace() . "\\db\\" . $this->modelClass->getClassname() . "Writer");
			$this->pathinfo($writerName->pathinfo($config->application->srcDir));

			$ns = $writerName->getNamespace();
			$tableName = trim($parameters[1]);
			$attributes = count($parameters) > 2 ? (strlen($parameters[2]) > 0 ? explode(',', $parameters[2]) : null) : null;

			$contents = str_replace(':namespace', $ns, $this->template);
			$contents = str_replace(':prefix', strtolower($this->modelClass->getClassname()), $contents);

			$contents = str_replace(':class', "class {$writerName->getClassname()}", $contents);

			$contents = str_replace(':model', $this->modelClass->getClassname(), $contents);
			$contents = str_replace(':table', $tableName, $contents);

			// create-method
			if(!empty($attributes)){
				$columns = implode(',', $attributes);
				$placeholders = implode(',:', $attributes);
				$contents = str_replace(':columns', $columns, $contents);
				$contents = str_replace(':placeholders', $placeholders, $contents);

				$params = [];
				foreach ($attributes as $attribute){
					array_push($params, "					'{$attribute}' => \$this->{$attribute},");
				}
				$contents = str_replace(':params', implode("\n", $params), $contents);
			}

			// update-method
			if(!empty($attributes)){
				$sets = [];
				foreach ($attributes as $attribute){
					array_push($sets, "					{$attribute} = :{$attribute}");
				}
				$contents = str_replace(':sets', implode(",\n", $sets), $contents);
			}

			// magic-get-cases
			if(!empty($attributes)){
				$cases = [];
				$template = "	    			case ':key':";
				foreach ($attributes as $attribute){
					array_push($cases, str_replace(':key', $attribute, $template));

				}
				$contents = str_replace(':cases', implode("\n", $cases), $contents);
			}

			// fill in import statements
			$imports = [];
			foreach ($this->imports as $import){
				$name = $import->fullname();
				array_push($imports, "\tuse {$name};");
			}
			$contents = str_replace(':uses', implode('\n', $imports), $contents);

			$this->writeToFile($contents);
		}


		function resetImports(array $imports = []){
			$this->imports = $imports;
		}
		function addImport(ClassName $import){
			array_push($this->imports, $import);
		}
		function removeImport(ClassName $import){
			$c = count($this->imports);
			for($i = 0; $i < $c; $i++){
				if($import->equals($this->imports[$i])){
					array_splice($this->imports, $i, 1);
					return;
				}
			}
		}
		function removeImportAt($index){
			unset($this->imports[$index]);
		}
		function importAt($index){
			return $this->imports[$index];
		}
		function indexOfImport(ClassName $import){
			return -1;
		}
		function hasImport(ClassName $import){
			foreach($this->imports as $instance){
				if($instance->equals($import)){
					return true;
				}
			}
			return false;
		}

		function imports(){
			return $this->imports;
		}
	}
}
