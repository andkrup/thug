<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\lang\ClassName;
	use supervillainhq\thugs\FileTask;

	/**
	 * Create or manage datawriter classes
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class MapperTask extends FileTask{

		private $template = <<<TEMPLATE
<?php
/**
 * Created by thugs
 *
 */
namespace :namespace{
	use supervillainhq\phalcon\db\DataMapper;
	use supervillainhq\phalcon\db\DataReader;
	use supervillainhq\phalcon\db\Mapper;
	use supervillainhq\phalcon\db\SqlQuery;
:uses

	:class extends Mapper implements DataMapper, DataReader{

		function __construct(\$data = null, \$lazyLoad = true){
			parent::__construct(\$data, \$lazyLoad);
	    	if(!is_null(\$data)){
	    		if(\$data instanceof :model){
	    			\$this->addParameter('id', \$data->id());
	    		}
	    		elseif (is_array(\$data)){
	    			\$this->resetParameters();
	    			\$keys = array_keys(\$data);
	    			foreach (\$keys as \$key){
	    				\$k = str_ireplace(':prefix_', '', \$key);
	    				\$this->addParameter(\$k, \$data[\$key]);
	    			}
	    		}
	    	}
		}
		
		function find(){
	    	\$searchables = ['id'];
	    	\$results = [];
	    	while (count(\$searchables) > 0){
	    		\$search = array_shift(\$searchables);
	    		if(\$this->hasParameterAtKey(\$search)){
	    			\$value = \$this->getParameter(\$search);
    				\$sql = "select
								:alias.id as :prefix_id,
:columns
							from :table :alias
							where :alias.{\$search} = :{\$search};";
    				\$query = SqlQuery::create(\$sql);
    				\$query->query(["{\$search}" => \$value]);
    				\$row = \$query->fetch();
    				if(isset(\$row)){
    					\$mapper = new :modelMapper((array) \$row);
    					if(\$instance = \$mapper->inflate()){
    						array_push(\$results, \$instance);
    					}
    				}
	    		}
	    	}
	    	return \$results;
	    }
		
		function get(){
	    	\$sql = "select
						:alias.id as :prefix_id,
:columns
					from :table :alias
					where :alias.id = :id;";
			\$query = SqlQuery::create(\$sql);
			\$query->query(["id" => \$this->getParameter('id')]);
			\$row = \$query->fetch();
			if(isset(\$row)){
				\$mapper = new :modelMapper((array) \$row);
				return \$mapper->inflate();
			}
			return null;
	    }
	    function exists(){
	    	\$sql = "select count(:alias.id) > 0 as existing
					from :table :alias
					where :alias.id = :id;";
			\$query = SqlQuery::create(\$sql);
			\$query->query(["id" => \$this->getParameter('id')]);
			\$exists = \$query->fetchValue('existing');
			return \$exists === 1;
	    }
	    function all(){
    		\$sql = "select
						:alias.id as :prefix_id,
:columns
					from :table :alias;";
	    	\$query = SqlQuery::create(\$sql);
	    	\$query->query();
	    	\$rows = \$query->fetchAll();
	    	\$instances = [];
	    	foreach (\$rows as \$row){
	    		\$mapper = new :modelMapper((array) \$row);
	    		if(\$instance = \$mapper->inflate()){
	    			array_push(\$instances, \$instance);
	    		}
	    	}
	    	return \$instances;
	    }
	    function reset(array \$data = null){}

	    function inflate(){
	    	return :model::inflate(\$this);
	    }

		function __get(\$name){
			if(\$this->hasParameterAtKey(\$name)){
				switch(\$name){
					case 'id':
						return intval(\$this->getParameter(\$name));
:cases
						return stripslashes(trim(\$this->getParameter(\$name)));
				}
			}
			return null;
		}
	}
}
TEMPLATE;


		private $imports;

		private $modelClass;


		public function mainAction(){
			echo "\nUsage: thugs mapper <model-class> <table-name> [column,...]\n";
		}

		public function helpAction(){
			echo "\nCreates a mapper class file. The model doesn't need to exist\n";
		}

		public function createAction(array $parameters = null){
			$this->resetImports();
			$config = $this->getDI()->getConfig();

			$modelName = $parameters[0];
			$this->modelClass = new ClassName($modelName);
			$this->addImport($this->modelClass);

			$writerName = new ClassName($this->modelClass->getNamespace() . "\\db\\" . $this->modelClass->getClassname() . "Mapper");
			$this->pathinfo($writerName->pathinfo($config->application->srcDir));

			$ns = $writerName->getNamespace();
			$tableName = trim($parameters[1]);
			$attributes = count($parameters) > 2 ? (strlen($parameters[2]) > 0 ? explode(',', $parameters[2]) : null) : null;
			$prefix = strtolower($this->modelClass->getClassname());
			$alias = strtolower(substr($prefix, 0, 1));

			$contents = str_replace(':namespace', $ns, $this->template);
			$contents = str_replace(':alias', $alias, $contents);
			$contents = str_replace(':prefix', $prefix, $contents);

			$contents = str_replace(':class', "class {$writerName->getClassname()}", $contents);

			$contents = str_replace(':model', $this->modelClass->getClassname(), $contents);
			$contents = str_replace(':table', $tableName, $contents);

			// select-columns
			$columns = [];
			if(!empty($attributes)){
				foreach ($attributes as $attribute){
					array_push($columns, "						{$alias}.{$attribute} as {$prefix}_{$attribute}");
				}
			}
			$contents = str_replace(':columns', implode(",\n", $columns), $contents);

			// magic-get-cases
			$cases = [];
			if(!empty($attributes)){
				$template = "	    			case ':key':";
				foreach ($attributes as $attribute){
					array_push($cases, str_replace(':key', $attribute, $template));

				}
			}
			$contents = str_replace(':cases', implode("\n", $cases), $contents);

			// fill in import statements
			$imports = [];
			foreach ($this->imports as $import){
				$name = $import->fullname();
				array_push($imports, "\tuse {$name};");
			}
			$contents = str_replace(':uses', implode('\n', $imports), $contents);

			$this->writeToFile($contents);
		}


		function resetImports(array $imports = []){
			$this->imports = $imports;
		}
		function addImport(ClassName $import){
			array_push($this->imports, $import);
		}
		function removeImport(ClassName $import){
			$c = count($this->imports);
			for($i = 0; $i < $c; $i++){
				if($import->equals($this->imports[$i])){
					array_splice($this->imports, $i, 1);
					return;
				}
			}
		}
		function removeImportAt($index){
			unset($this->imports[$index]);
		}
		function importAt($index){
			return $this->imports[$index];
		}
		function indexOfImport(ClassName $import){
			return -1;
		}
		function hasImport(ClassName $import){
			foreach($this->imports as $instance){
				if($instance->equals($import)){
					return true;
				}
			}
			return false;
		}

		function imports(){
			return $this->imports;
		}
	}
}
