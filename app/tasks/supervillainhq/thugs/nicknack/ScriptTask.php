<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\FileTask;
	use supervillainhq\thugs\io\CliOutput;

	/**
	 * Created by ak
	 */
	class ScriptTask extends FileTask{

		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		public function listAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nLists all current scripts used on the website");
		}

		public function addAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nAdd a script");
		}

		public function removeAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nRemove a script");
		}
	}
}
