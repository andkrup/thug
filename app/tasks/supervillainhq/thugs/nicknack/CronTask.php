<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\FileTask;
	use supervillainhq\thugs\io\CliOutput;

	/**
	 * Created by ak
	 */
	class CronTask extends FileTask{

		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		public function listAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nLists all current crontabs related to the website");
		}

		public function addAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nAdd a crontab");
		}

		public function removeAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nStop a crontab");
		}

		public function testAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nTest a crontab");
		}
	}
}
