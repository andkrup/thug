<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\cli\CliOutput;
	use supervillainhq\thugs\FileTask;

	/**
	 * Created by ak
	 */
	class ProjectTask extends FileTask{

		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		public function scaffoldAction(array $parameters = null){
			// remember .phalcon folder (https://forum.phalconphp.com/discussion/1143/error-this-command-should-be-invoked-inside-a-phalcon-project-di)
			$structure = [
				'root' => [
					'app' => [
						'src',
						'frontend' => [
							'controllers',
							'views',
						]
					],
					'config' => [
						'classmap.php',
						'config.php',
						'loader.php',
						'services.php',
					],
					'public' => [
						'index.php'
					],
					'vagrant' => [
						'.apache',
						'.xdebug',
					],
					'vendor',
					'resources',
					'migrations',
					'.phalcon',
				]
			];
			$cwd = getcwd();
			$this->createSubDirectories($cwd, $structure['root']);
		}

		private function createSubDirectories($cwd, array $paths){
			$output = new CliOutput();
			foreach ($paths as $file => $contents){
				if(is_integer($file)){
					$file = $contents;
					$contents = null;
				}
				$pathinfo = (object) pathinfo("{$cwd}/{$file}");
				$filepath = "{$pathinfo->dirname}/{$pathinfo->basename}";
				if(property_exists($pathinfo, 'extension') && $pathinfo->extension == 'php'){
					$output->line("creating file: {$filepath}");
					if(!is_file($filepath)){
						$this->writeToFile('', $pathinfo);
					}
				}
				else{
					$output->line("creating folder: {$filepath}");
					if(!is_dir($filepath)){
						$this->createPath($pathinfo);
					}
					if(is_array($contents)){
						$this->createSubDirectories("{$filepath}", $contents);
					}
				}
			}

		}

		public function phpunitAction(array $parameters = null){
			// add required files (bootstrap, config xml, etc)
			$output = new CliOutput();
			$output->line("\nInitiate phpunit files and dependencies");

			$cwd = getcwd();

			$this->createPath((object) pathinfo("{$cwd}/resources"));

			$pathBootstrapPhar = "{$cwd}/vendor/supervillainhq/phpbasics/build/bootstrap.phar";
			$pathBootstrapPharTarget = "{$cwd}/resources/bootstrap.phar";
			if(is_readable($pathBootstrapPhar)){
				copy($pathBootstrapPhar, $pathBootstrapPharTarget);
			}

			$pathBootstrap = "{$cwd}/vendor/supervillainhq/phpbasics/phpunit.php";
			$pathBootstrapTarget = "{$cwd}/phpunit.php";
			if(is_readable($pathBootstrap)){
				copy($pathBootstrap, $pathBootstrapTarget);
			}

			$pathBootstrapCfg = "{$cwd}/vendor/supervillainhq/phpbasics/phpunit.xml";
			$pathBootstrapCfgTarget = "{$cwd}/phpunit.xml";
			if(is_readable($pathBootstrapCfg)){
				copy($pathBootstrapCfg, $pathBootstrapCfgTarget);
			}
			// ensure composer dependency
		}

		public function phingAction(array $parameters = null){
			// add required files (.phing/build.properties, .phing/version)
			$output = new CliOutput();
			$output->line("\nInitiate phing files and dependencies");

			$cwd = getcwd();

			$phingDir = "{$cwd}/.phing";

			$pathBuildFile = "{$cwd}/vendor/supervillainhq/phpbasics/build.xml";
			$pathBuildFileTarget = "{$cwd}/build.xml";
			if(is_readable($pathBuildFile)){
				copy($pathBuildFile, $pathBuildFileTarget);
			}

			$this->writeToFile('', (object) pathinfo("{$phingDir}/build.properties"));
			$this->writeToFile('0.0.1', (object) pathinfo("{$phingDir}/version"));
		}

		public function spectreAction(array $parameters = null){
			echo "\nThis will add the spectre cms and web components to the project \n";
		}

		public function lexcorpAction(array $parameters = null){
			echo "\nThis will add the lexcorp shop components to the project \n";
		}

		public function arkhamAction(array $parameters = null){
			echo "\nThis will add the core and arkham components to the project \n";
		}
	}
}
