<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\FileTask;

	/**
	 * Create or manage model classes
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class EnvTask extends FileTask{
		static $defaultConfigs = [
			'application',
			'locale',
			'scripts',
			'stylesheets',
			'database',
		];
		static $prefabConfigs = [
			'googleanalytics',
			'facebook',
			'gettext',
		];
		protected $selectedEnv;
		protected $folderName = 'env';
		protected $configFiles = [];
		protected $availableEnvs = [];


		public function mainAction(){
		}

		public function initAction(array $parameters = null){
			$environments = [];
			if(count($parameters)>0){
				$environments = explode(',', $parameters[0]);
			}
			$this->configFiles = array_merge($this->configFiles, self::$defaultConfigs);
			if(count($parameters)>1){
				$this->configFiles = array_merge($this->configFiles, explode(',', $parameters[1]));
			}
			if(count($parameters)>2){
				$this->folderName = trim($parameters[2]);
			}
			$dirname = getcwd() . "/{$this->folderName}";
			$this->pathinfo((object) pathinfo($dirname));
			$this->createPath();

			// create subdirectories for every environment
			foreach ($environments as $environment){
				$dirname = getcwd() . "/env/{$environment}";
				$pathInfo = (object) pathinfo($dirname);
				$this->createPath($pathInfo);

				// create empty json files
				foreach ($this->configFiles as $jsonfile) {
					$filename = "{$dirname}/config/{$jsonfile}.json";
					$this->addConfig($filename);
				}
			}

		}

		protected function addConfig($fullpath, array $contents = []){
			$pathInfo = (object) pathinfo($fullpath);
			if(is_null($contents)){
				$contents = [];
			}
			$contents = json_encode((object) $contents);
			$this->writeToFile($contents, $pathInfo);
		}

		/**
		 * Sync 2 environments
		 * @param array|null $parameters
		 */
		public function syncAction(array $parameters = null){
			if(count($parameters)>1){
				$sourceEnv = trim($parameters[0]);
				$targetEnv = trim($parameters[1]);

				$targetFiles = [];
				$sourceFiles = [];

				// list all files in targetEnv
				$targetEnvPath = getcwd() . "/{$this->folderName}/{$targetEnv}";
				$files = glob("{$targetEnvPath}/config/*.json");
				foreach($files as $file){
					if(is_file($file) && is_readable($file)){
						$pathinfo = (object) pathinfo($file);
						array_push($targetFiles, $pathinfo->basename);
					}
				}

				// list all files in sourceEnv
				$sourceEnvPath = getcwd() . "/{$this->folderName}/{$sourceEnv}";
				$files = glob("{$sourceEnvPath}/config/*.json");
				foreach($files as $file){
					if(is_file($file) && is_readable($file)){
						$pathinfo = (object) pathinfo($file);
						if(!in_array($pathinfo->basename, $targetFiles)){
							array_push($sourceFiles, $pathinfo->basename);
							copy($file, "{$targetEnvPath}/{$pathinfo->basename}");
						}
					}
				}
				$sourceFiles = implode("\n", $sourceFiles);
				echo "\ncopied files from '{$sourceEnv}' to '{$targetEnv}':\n{$sourceFiles}\n";
			}
		}

		public function listAction(array $parameters = null){
			$envs = implode("\n  - ", $this->listEnvironments());
			echo "\nEnvironments:\n  - {$envs}\n";
		}
		protected function listEnvironments(){
			$this->availableEnvs = [];
			$envPath = getcwd() . "/{$this->folderName}";
			$files = glob("{$envPath}/*");
			foreach($files as $file){
				if(is_dir($file)){
					array_push($this->availableEnvs, basename($file));
				}
			}
			return $this->availableEnvs;
		}

		public function updateAction(array $parameters = null){
			if(count($parameters)>2){
				$subCommand = trim($parameters[1]);
				$targetEnv = trim($parameters[0]);
				$configFile = trim($parameters[2]);

				$filename = getcwd() . "/env/{$targetEnv}/config/{$configFile}.json";
				switch ($subCommand){
					case 'add':
						echo "\nAdd file '{$filename}'\n";
						$this->addConfig($filename);
						break;
					case 'remove':
						echo "\nRemove file '{$filename}'\n";
						break;
					case '':
					default:
						break;
				}
			}
		}

		public function configAction(array $parameters = null){
		}

		/**
		 * Create a config for facebook with ready-made keys
		 *
		 * @param array|null $parameters
		 */
		public function facebookAction(array $parameters = null){
			$environments = [];
			if(count($parameters)>0){
				$environments = explode(',', $parameters[0]);
			}
			if(count($parameters)>1){
				$this->folderName = trim($parameters[1]);
			}

			$fbConfigs = [
				'app_id' => '',
				'app_secret' => '',
				'default_graph_version' => 'v2.5',
			];

			$this->configFiles = array_merge($this->configFiles, ['facebook']);

			$dirname = getcwd() . "/{$this->folderName}";
			$this->pathinfo((object) pathinfo($dirname));
			$this->createPath();

			// create subdirectories for every environment
			foreach ($environments as $environment){
				$dirname = getcwd() . "/env/{$environment}";
				$pathInfo = (object) pathinfo($dirname);
				$this->createPath($pathInfo);

				// create empty json files
				$filename = "{$dirname}/config/facebook.json";
				$this->addConfig($filename, $fbConfigs);
			}
		}

		/**
		 * Create a config for google-analytics with ready-made keys
		 *
		 * @param array|null $parameters
		 */
		public function googleanalyticsAction(array $parameters = null){
		}

		/**
		 * Create a config for gettext with ready-made keys
		 *
		 * @param array|null $parameters
		 */
		public function gettextAction(array $parameters = null){
		}

		/**
		 * Change the current environment used to any of the existing environments
		 * @param array|null $parameters
		 */
		public function selectAction(array $parameters = null){
			if(count($parameters)>0){
				$this->selectedEnv = trim($parameters[0]);
			}

			// clear files in env/
			$targetEnvPath = getcwd() . "/{$this->folderName}";
			$files = glob("{$targetEnvPath}/*.json");
			foreach($files as $file){
				if(is_file($file) && is_writable($file)){
					unlink($file);
				}
			}

			// copy env/<environment>/config/*.json to env/
			$sourceEnvPath = getcwd() . "/{$this->folderName}/{$this->selectedEnv}";
			$files = glob("{$sourceEnvPath}/config/*.json");
			foreach($files as $file){
				if(is_file($file) && is_readable($file)){
					$fileInfo = (object) pathinfo($file);
					$filename = $fileInfo->basename;
					copy($file, "{$targetEnvPath}/{$filename}");
				}
			}
		}
	}
}
