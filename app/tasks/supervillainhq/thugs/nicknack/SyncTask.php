<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;

	/**
	 * Synchronise files or folders between a local and remote environment.
	 *
	 * User: ak
	 */
	class SyncTask extends Task{

		/**
		 * Synchronise uploaded resources between your local development environment and a remote environment.
		 *
		 * @param array|null $parameters
		 */
		public function uploadsAction(array $parameters = null){
		}

		/**
		 * Synchronise configurations between a remote environment and the copy in your local development env folder.
		 * @param array|null $parameters
		 */
		public function configAction(array $parameters = null){
		}

		/**
		 * Synchronise cache values between a remote environment and your local development env folder.
		 * @param array|null $parameters
		 */
		public function cacheAction(array $parameters = null){
		}

		/**
		 * Synchronise database values between a remote environment and your local development env folder.
		 *
		 * @param array|null $parameters
		 */
		public function databaseAction(array $parameters = null){
		}
	}
}
