<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\FileTask;
	use supervillainhq\thugs\cli\CliOutput;

	/**
	 * Manage the classmap required for the object-factories
	 * Created by ak
	 */
	class ClassMapTask extends FileTask{


		private $template = <<<TEMPLATE
<?php
/**
 * Auto-generated by nicknack classmap
 *
 */
return :map;
TEMPLATE;

		protected $classMap;


		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		protected function classmapPath(){
			$cwd = getcwd();
			return "{$cwd}/config/classmap.php";
		}

		protected function loadClassMap($classmapPath = null){
			if(is_null($classmapPath)){
				$classmapPath = $this->classmapPath();
			}
			if (is_readable($classmapPath)) {
				$aliases = include $classmapPath;
				$this->resetClassMap($aliases);
			}
		}

		public function createAction(array $parameters = null){
			$classmapPath = $this->classmapPath();
			if (is_readable($classmapPath)) {
				return;
			}
			$this->resetClassMap();
			$this->updateClassmap($classmapPath);
		}
		protected function updateClassmap($classmapPath = null){
			if(is_null($classmapPath)){
				$classmapPath = $this->classmapPath();
			}
			$map = "\n";
			$contexts = $this->classMap();
			foreach ($contexts as $key => $context){
				if(is_array($context)){
					$map .= "\t'{$key}' => [\n";
					foreach ($context as $alias => $classpath) {
						$map .= "\t\t'{$alias}' => '{$classpath}',\n";
					}
					$map .= "\t],\n";
				}
				else{
//					foreach ($aliases as $alias => $classpath) {
//						$map .= "\t'{$alias}' => '{$classpath}',\n";
//					}
					$alias = $key;
					$classpath = $context;
					$map .= "\t'{$alias}' => '{$classpath}',\n";
				}
			}
			$contents = str_replace(':map', "[{$map}]", $this->template);
			$this->writeToFile($contents, pathinfo($classmapPath));
		}

		public function listAction(array $parameters = null){
			$this->loadClassMap();
			$output = new CliOutput();
			$output->line("\n<header1>Classes currently mapped:</header1>\n");
			foreach ($this->classMap as $context => $classpaths){
				$output->line("\n<emphasis>{$context} :</emphasis>");
				foreach ($classpaths as $alias => $classpath) {
					$output->line("    '{$alias}' : '{$classpath}'");
				}
			}
		}

		public function addAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nAdd a class alias to the map");

			$this->loadClassMap();
			$alias = trim($parameters[0]);
			$classpath = trim($parameters[1]);
			$context = isset($parameters[2]) ? trim($parameters[2]) : null;
			$this->addClassAlias($alias, $classpath, $context);
			$this->updateClassmap();
		}

		public function removeAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nRemove a class alias from the map");

			$this->loadClassMap();
			$alias = trim($parameters[0]);
//			$classpath = trim($parameters[1]);
			$context = isset($parameters[1]) ? trim($parameters[1]) : null;
			$this->removeClassAliasAt($alias, $context);
			$this->updateClassmap();
		}

		public function testAction(array $parameters = null){
			$output = new CliOutput();
			$output->line("\nTest that a class can be instantiated from the map");
		}
		

		function resetClassMap(array $classMap = []){
			$this->classMap = $classMap;
		}
		function addClassAlias($alias, $classpath, $context = null){
			$this->loadClassMap();
			if(is_null($context)){
				$this->classMap[$alias] = $classpath;
			}
			else{
				if(!array_key_exists($context, $this->classMap)){
					$this->classMap[$context] = [];
				}
				$this->classMap[$context][$alias] = $classpath;
			}
			$this->updateClassmap();
		}
		function removeClassAlias($classpath, $context = null){
			if(is_null($context)){
				foreach($this->classMap as $alias => $path){
					if(0 === strcmp($path, $classpath)){
						unset($this->classMap[$alias]);
						return;
					}
				}
			}
			else{
				if(array_key_exists($context, $this->classMap)){
					foreach($this->classMap[$context] as $alias => $path){
						if(0 === strcmp($path, $classpath)){
							unset($this->classMap[$context][$alias]);
							return;
						}
					}
				}
			}
		}
		function removeClassAliasAt($alias, $context = null){
			if(is_null($context)){
				unset($this->classMap[$alias]);
			}
			else{
				if(array_key_exists($context, $this->classMap)){
					unset($this->classMap[$context][$alias]);
				}
			}
		}
		function getClassPath($alias){
			return $this->classMap[$alias];
		}
		function hasClassAliasAt($alias){
			return isset($this->classMap[$alias]);
		}
		function classPathExists($classpath){
			foreach($this->classMap as $alias => $path){
				if(0 === strcmp($path, $classpath)){
					return true;
				}
			}
			return false;
		}
		
		function classMap(){
			return $this->classMap;
		}
	}
}
