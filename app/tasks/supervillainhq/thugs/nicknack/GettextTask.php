<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;

	/**
	 * Created by ak
	 */
	class GettextTask extends Task{

		/**
		 * Have gettext parse source files and update labels.
		 *
		 * @param array|null $parameters
		 */
		public function updateDomainAction(array $parameters = null){
		}

		/**
		 * Build mo files
		 * @param array|null $parameters
		 */
		public function buildMoAction(array $parameters = null){
		}
	}
}
