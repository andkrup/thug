<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\FileTask;

	/**
	 * Manage a git repo in a remote environment and distribute checkouts as releases.
	 *
	 * User: ak
	 */
	class DeployTask extends FileTask{

		/**
		 * Clone a repository into a remote environment. This repo can then be updated to a specific commit, and the
		 * desired files can then be copied into a release folder
		 * @param array $parameters
		 */
		public function initRemoteAction(array $parameters = null){
			// Checkout in remote env:
			// - create deploy-user, if needed
			// - create /var/git/<projectpath>, if needed
			// - clone git repo from origin
			// - create shared resources (uploads-folder, etc.)
		}

		/**
		 * Deploy a new release
		 *
		 * @param array|null $parameters
		 */
		public function releaseAction(array $parameters = null){
			// Pull commits from origin
			// Check out desired commit
			// Create release folder
			// Copy files to release folder
			// Change current symlink
			// Create symlinks to shared resources
			// Update dependencies and caches, if required
		}

		/**
		 * Undo a deploy
		 * @param array $parameters
		 */
		public function rollbackAction(array $parameters = null){
			// change the current symlink to point to a previous release
		}
	}
}
