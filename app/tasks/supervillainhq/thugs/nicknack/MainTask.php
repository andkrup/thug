<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\cli\CliOutput;

	/**
	 * Created by ak
	 */
	class MainTask extends Task{
		private $version = '0.1';

		public function mainAction(){
			$output = new CliOutput();
			$output->line("\n<header1>SupervillainHQ Nicknack version {$this->version}</header1>\n");
			$output->line("This Nick nack version will never send assassins to kill you!\n");
		}

		public function help(){
			$output = new CliOutput();
			$output->line("\n<header1>SupervillainHQ Nicknack version {$this->version}</header1>\n");
			$output->line("NickNack commands and options reference\n");
			$output->line('<header2>Commands</header2>');
			$output->line("    class, config, env, mapper, writer");
			$output->line("\n<emphasis>The Class command</emphasis>\n");
			$output->line("The class command creates a new php class for use in the business model. Classes are created using the DataAware trait and has a static inflate() method so any mapper objects easily can create instances.\n");
			$output->line("<bold>Available arguments:</bold>");
			$output->line("    <code>create</code> - Creates a new class");
			$output->line("\n    <bold>Usage:</bold>");
			$output->line("        <code>nicknack class create [classname] [superclasses,...] [interfaces,...] [traits,...]</code>\n");
			$output->line("\n<emphasis>Mapper command</emphasis>\n");
			$output->line("The mapper command creates a new mapper class, handling data-mapping for the specified business model class.\n");
			$output->line("<bold>Available arguments:</bold>");
			$output->line("    <code>create</code> - Creates a new mapper class");
			$output->line("\n    <bold>Usage:</bold>");
			$output->line("        <code>nicknack mapper create [model] [tablename] [attributes,...]</code>\n");
			$output->line("\n<emphasis>Writer command</emphasis>\n");
			$output->line("The writer command creates a new writer class, handling data-writing for the specified business model class.\n");
			$output->line("<bold>Available arguments:</bold>");
			$output->line("    <code>create</code> - Creates a new writer class");
			$output->line("\n    <bold>Usage:</bold>");
			$output->line("        <code>nicknack writer create [model] [tablename] [attributes,...]</code>\n");
			$output->line("\n<emphasis>Env command</emphasis>\n");
			$output->line("The env command manages configuration files and values for the different environments that the project must support.\n");
			$output->line("<bold>Available arguments:</bold>");
			$output->line("    <code>init</code> - Initiates the env/ folder structure. Each subfolder may contain environment settings for a different environment");
			$output->line("    <code>list</code> - Lists all available environments (subfolders in env/)");
			$output->line("    <code>select</code> - Copies json files from a specified environment into env/");
			$output->line("    <code>update</code> - Adds or removes json-files to a specified environment");
			$output->line("    <code>sync</code> - Copies all json files not found in target-environment from source-another");
			$output->line("\n    <bold>Usage:</bold>");
			$output->line('        <code>nicknack env init [environments,...] [configs,...]</code>');
			$output->line('        <code>nicknack env list</code>');
			$output->line('        <code>nicknack env select [environment]</code>');
			$output->line('        <code>nicknack env update [environment] [add|remove] [config]</code>');
			$output->line('        <code>nicknack env sync [source-environment] [target-environment]</code>');
			$output->line("\n<emphasis>Config command</emphasis>\n");
			$output->line("The config command transfers environment-specific values to the application configuration.\n");
			$output->line("<bold>Available arguments:</bold>");
			$output->line("    <code>show</code> - Displays the current value(s) a specific config has.");
			$output->line("    <code>reset</code> - Reads all json files in the current environment and dumps the contents as an application configuration php-file");
			$output->line("\n    <bold>Usage:</bold>");
			$output->line('        <code>nicknack config reset</code>');
			$output->line('        <code>nicknack config show</code>');
//			$output->line('<bold>Env command</bold>');
//			$output->line('    <code>nicknack class create</code>');
//			$output->line("    - Copies all json files from one environment to another\n");
		}
	}
}
