<?php

namespace supervillainhq\thugs\nicknack{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\FileTask;
	use supervillainhq\thugs\cli\CliOutput;

	/**
	 * Created by ak
	 */
	class PhpUnitTask extends FileTask{

		public function mainAction(){
			echo "\nactions:\n";
			echo "    create\n";
			echo "\nparameters:\n";
			echo "    model\n";
		}

		public function testAction(array $parameters = null){
			//
			$output = new CliOutput();
			$output->line("\nCreate a new test");
		}

		public function suiteAction(array $parameters = null){
			//
			$output = new CliOutput();
			$output->line("\nCreate a new test suite");
		}

		public function mockAction(array $parameters = null){
			//
			$output = new CliOutput();
			$output->line("\nCreate a new test");
		}
	}
}
