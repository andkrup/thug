<?php

namespace supervillainhq\thugs\oddjobtasks{
	use Phalcon\Cli\Task;
	use supervillainhq\thugs\cli\CliOutput;

	/**
	 * Created by PhpStorm.
	 * User: ak
	 * Date: 22/03/16
	 * Time: 10:53
	 */
	class TestTask extends Task{
		public function mainAction($parameters = null){
//			var_dump($parameters);exit;
//			$task = trim(array_shift($parameters));
//			$args = implode(', ', $parameters);
			$output = new CliOutput();
			$output->line("\n<header1>TESTTASK</header1>\n");
		}

		public function help(){
			$output = new CliOutput();
			$output->line("\n<header1>SupervillainHQ Oddjob version 0.1</header1>\n");
			$output->line("Oddjob commands and options\n");
			$output->line('<header2>Commands</header2>');
			$output->line("    class, config, env, mapper, writer");
			$output->line("\n<emphasis>The Class command</emphasis>\n");
			$output->line("The class command creates a new php class for use in the business model. Classes are created using the DataAware trait and has a static inflate() method so any mapper objects easily can create instances.\n");
			$output->line("<bold>Available arguments:</bold>");
			$output->line("    <code>create</code> - Creates a new class");
			$output->line("\n    <bold>Usage:</bold>");
			$output->line("        <code>thugs class create [classname] [superclasses,...] [interfaces,...] [traits,...]</code>\n");
			$output->line("\n<emphasis>Mapper command</emphasis>\n");
			$output->line("The mapper command creates a new mapper class, handling data-mapping for the specified business model class.\n");
			$output->line("<bold>Available arguments:</bold>");
			$output->line("    <code>create</code> - Creates a new mapper class");
			$output->line("\n    <bold>Usage:</bold>");
			$output->line("        <code>thugs mapper create [model] [tablename] [attributes,...]</code>\n");
		}
	}
}
