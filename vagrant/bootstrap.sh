#!/usr/bin/env bash

if [[ -d /vagrant/.xdebug ]]
	then
	rm -rf /vagrant/.xdebug
fi
mkdir -p /vagrant/.xdebug

## fix missing locales for ssh session
if ! locale -a | grep -q ^en_US
	then
	echo "generating en_US.UTF-8"
	locale-gen en_US.UTF-8
fi

sed -i '/AcceptEnv LANG LC_*/c\#AcceptEnv LANG LC_*' /etc/ssh/sshd_config

if ! grep -Fxq 'export LC_ALL="en_US.UTF-8"' /home/vagrant/.bashrc
	then
	echo 'export LC_ALL="en_US.UTF-8"' >> /home/vagrant/.bashrc
fi
if ! grep -Fxq 'export LANGUAGE="en_US.UTF-8"' /home/vagrant/.bashrc
	then
	echo 'export LANGUAGE="en_US.UTF-8"' >> /home/vagrant/.bashrc
fi
if ! grep -Fxq 'export LANG="en_US.UTF-8"' /home/vagrant/.bashrc
	then
	echo 'export LANG="en_US.UTF-8"' >> /home/vagrant/.bashrc
fi
if ! grep -Fxq 'export LC_MESSAGES="en_US.UTF-8"' /home/vagrant/.bashrc
	then
	echo 'export LC_MESSAGES="en_US.UTF-8"' >> /home/vagrant/.bashrc
fi

# add required repos (phalcon)
#apt-get install python-software-properties
apt-add-repository ppa:phalcon/stable -y
apt-add-repository ppa:chris-lea/redis-server -y
apt-get update

# download and install required software
apt-get install php5 git php5-xdebug php5-phalcon php5-curl nodejs npm -y

## disable xdebug in php cli
#if [[ -L /etc/php5/cli/conf.d/20-xdebug.ini ]]
#	then
#	echo "disabling php-xdebug in cli-mode"
#	unlink /etc/php5/cli/conf.d/20-xdebug.ini
#fi

# download & install composer
#cd /usr/local/bin
if [[ ! -f /usr/local/bin/composer ]]
	then
	echo "downloading & installing composer"
	curl -sS https://getcomposer.org/installer | php -- --install-dir=/home/vagrant
	mv /home/vagrant/composer.phar /usr/local/bin/composer
	chmod +x /usr/local/bin/composer
fi

# fix missing node symlink
if [[ ! -L /usr/local/bin/node ]]
	then
	echo "updating node symlink"
	ln -s /usr/bin/nodejs /usr/local/bin/node
fi

## add our custom php settings
#ln -fs /vagrant/php.ini /etc/php5/apache2/conf.d/customsettings.ini

# install composer dependencies
cd /vagrant_root
composer install

# install/configure phalcon devtools
if [[ ! -L /usr/bin/phalcon ]]
	then
	echo "updating phalcon devtools symlink"
	ln -s /vagrant_root/vendor/phalcon/devtools/phalcon.php /usr/bin/phalcon
	chmod +x /usr/bin/phalcon
fi
