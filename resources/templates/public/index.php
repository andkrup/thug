<?php
# composer autoload
include __DIR__ . '/../vendor/autoload.php';

use supervillainhq\spectre\db\SqlQuery;
use supervillainhq\core\date\Date;

error_reporting(E_ALL);

// disable DOMPDF's internal autoloader if you are using Composer
define('DOMPDF_ENABLE_AUTOLOAD', false);

// include DOMPDF's default configuration
require_once __DIR__ . '/../vendor/dompdf/dompdf/dompdf_config.inc.php';

try {

    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/../config/config.php";

    /**
     * Read auto-loader
     */
    include __DIR__ . "/../config/loader.php";

    /**
     * Read services
     */
    include __DIR__ . "/../config/services.php";

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

	$application->registerModules(
		[
			'cms' => [
				'className' => 'supervillainhq\spectre\cms\PhalconCms',
				'path'      => '../vendor/supervillainhq/phpalconcms/php/src/supervillainhq/spectre/cms/PhalconCms.php',
			],
			'frontend' => [
				'className' => 'supervillainhq\tobissen\Tobissen',
				'path'      => '../app/src/supervillainhq/tobissen/Tobissen.php',
			],
			'api'  => [
				'className' => 'supervillainhq\tobissen\api\TobissenAPI',
				'path'      => '../app/src/supervillainhq/tobissen/api/TobissenAPI.php',
			],
			'dashboard'  => [
				'className' => 'supervillainhq\phalcon\account\AccountModule',
				'path'      => '../app/src/supervillainhq/phalcon/account/AccountModule.php',
			],
			'admin'  => [
				'className' => 'supervillainhq\tobissen\admin\TobissenAdmin',
				'path'      => '../app/src/supervillainhq/tobissen/admin/TobissenAdmin.php',
			]
		]
	);
    // static reference to the current dbadapter
    SqlQuery::$dbAdapter = $application->db;
    // set up datetimeutil
    Date::config((array) $config->localization);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage();
}
