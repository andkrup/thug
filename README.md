# README #

## Thug ##

Command line tasks helping evil masterminds in their rapid website development

## Install ##

Add the supervillainhq repository to the composer.json:

```

	"config":{
		"secure-http" : false,
		"cache-files-ttl": 0
	},
	"repositories": [
		{
		    "type": "composer",
		    "url": "http://satis.supervillainhq.net"
		}
	],
	"require-dev": {
		"supervillainhq/thug": "dev-master",
		"phalcon/devtools": "dev-master"
	}


```

Install by having composer update from the json file

```

$ composer update

```